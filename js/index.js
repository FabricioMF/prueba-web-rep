
    $(function () {
        $('[data-toggle="tooltip"]').tooltip();
        $('[data-toggle="popover"]').popover();
        $(".carousel").carousel({
            interval: 1000,
        });
        $("#modalContacto").on("show.bs.modal", function (e) {
            console.log("Se esta abriendo el modal");
            $("#contactoBtn").removeClass("btn-primary");
            $("#contactoBtn").addClass("btn-success");
            $("#contactoBtn").prop("disabled", true);
        });
        $("#modalContacto").on("shown.bs.modal", function (e) {
            console.log("Se abrio el modal");
        });
        $("#modalContacto").on("hide.bs.modal", function (e) {
            console.log("Se esta cerrando el modal");
        });
        $("#modalContacto").on("hidden.bs.modal", function (e) {
            console.log("Se cerro el modal");
            $("#contactoBtn").prop("disabled", false);
            $("#contactoBtn").removeClass("btn-success");
            $("#contactoBtn").addClass("btn-primary");
        });
    });

